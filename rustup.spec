%global rustflags '-Clink-arg=-Wl,-z,relro,-z,now'
%global min_rust_version 1.51.0

Name:           rustup
Version:        1.24.3
Release:        0
Summary:        The rust toolchain installer
License:        Apache-2.0 OR MIT
URL:            https://github.com/rust-lang/rustup
Source:         %{name}-%{version}.tar.gz

BuildRequires:  cargo >= %{min_rust_version}
BuildRequires:  pkgconfig(openssl)

%description
Rustup installs The Rust Programming Language from the official release channels, enabling you to easily switch between stable, beta, and nightly compilers and keep them updated. It makes cross-compiling simpler with binary builds of the standard library for common platforms. And it runs on all platforms Rust supports, including Windows.

%prep
%setup -q -n %{name}-%{version}
tar -xf vendor.tar.gz

%build
RUSTFLAGS=%{rustflags} cargo build --release --features=no-self-update

%install
install -D -d -m 0755 %{buildroot}%{_bindir}
install -m 0755 %{_builddir}/%{name}-%{version}/target/release/rustup-init %{buildroot}%{_bindir}/rustup
ln -sf rustup %{buildroot}%{_bindir}/rustup-init
install -D -d -m 0755 %{buildroot}%{_datadir}/bash-completion/completions
ls %{buildroot}%{_bindir}
ls %{buildroot}%{_bindir}/rustup
%{buildroot}%{_bindir}/rustup completions bash > %{buildroot}%{_datadir}/bash-completion/completions/rustup
install -D -d -m 0755 %{buildroot}%{_datadir}/zsh/site-functions
%{buildroot}%{_bindir}/rustup completions zsh > %{buildroot}%{_datadir}/zsh/site-functions/_rustup

%files
%{_bindir}/rustup
%{_bindir}/rustup-init
%{_datadir}/bash-completion/completions/rustup
%{_datadir}/zsh/site-functions/_rustup

%changelog
